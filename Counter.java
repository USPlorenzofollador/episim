


public class Counter {

	public static int numberOfSusceptible;
	public static int numberOfInfected;
	public static int numberOfRecovered;
	public static int numberOfImmune;

	// methods that computes the numberof S, I or R
		
	public static int countSusceptible(ParticleSIR[] particles) {

		int susceptibleNumber = 0;

		for (int i = 0; i < particles.length; i++) {
			
			if (particles[i].susceptible && !particles[i].infected) {
				susceptibleNumber++;
			}
		}

		return susceptibleNumber;
	}

//----------------------------------------------------------------------

	public static int countInfected(ParticleSIR[] particles) {

		int infectedNumber = 0;

		for (int i = 0; i < particles.length; i++) {
			
			if (particles[i].infected) {
				infectedNumber++;
			}
		}

		return infectedNumber;
	}

//----------------------------------------------------------------------

	public static int countRecovered(ParticleSIR[] particles) {

		int recoveredNumber = 0;

		for (int i = 0; i < particles.length; i++) {
			
			if (particles[i].recovered) {
				recoveredNumber++;
			}
		}

		return recoveredNumber;
	}

//======================================================================

}