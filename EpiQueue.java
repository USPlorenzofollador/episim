/******************************************************************************
 *  Compilation:  javac-introcs EpiQueue.java
 *  Execution: java-introcs EpiQueue 100 90 46
 ******************************************************************************/

import java.awt.Color;

/**
 *  The {@code EpiQueue} class represents a collection of particles
 *  moving in the unit box, according to the laws of elastic collision.
 *  This event-based simulation relies on a priority queue.
 *  <p>
 *  For additional documentation, 
 *  see <a href="https://algs4.cs.princeton.edu/61event">Section 6.1</a> of 
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne. 
 *
 *  @author Eduardo Seiji
 *  @author Gabriel Leão
 *  @author Lorenzo Follador
 */
public class EpiQueue {
    private static final double HZ = 1.5;    // redraw events per clock tick

    public MinPQ<Event> pq;          // the priority queue
    public static double t  = 0.0;          // simulation clock time
    private ParticleQ[] particles;     // the array of particles

    public double recoveringTime = 1000;

    public static int numberOfIterations = 0;

    /**
     * Initializes a system with the specified collection of particles.
     * The individual particles will be mutated during the simulation.
     *
     * @param  particles the array of particles
     */
    public EpiQueue(ParticleQ[] particles) {
        this.particles = particles.clone();   // defensive copy
    }

    // updates priority queue with all new events for particle a
    private void predict(ParticleQ a, double limit) {
        if (a == null) return;

        // particle-particle collisions
        for (int i = 0; i < particles.length; i++) {
            double dt = a.timeToHit(particles[i]);
            if (t + dt <= limit)
                pq.insert(new Event(t + dt, a, particles[i]));
        }

        // particle-wall collisions
        double dtX = a.timeToHitVerticalWall();
        double dtY = a.timeToHitHorizontalWall();
        if (t + dtX <= limit) pq.insert(new Event(t + dtX, a, null));
        if (t + dtY <= limit) pq.insert(new Event(t + dtY, null, a));
    }

    // redraw all particles
    private void redraw(double limit) {
        StdDraw.clear();
        for (int i = 0; i < particles.length; i++) {
            particles[i].draw();
        }
        StdDraw.show();
        StdDraw.pause(20);
        if (t < limit) {
            pq.insert(new Event(t + 1.0 / HZ, null, null));
        }
    }

    /**
     * Simulates the system of particles for the specified amount of time.
     *
     * @param  limit the amount of time
     */

    public void simulate(double limit) {

      // initialize PQ with collision events and redraw event
        pq = new MinPQ<Event>();
        for (int i = 0; i < particles.length; i++) {
            predict(particles[i], limit);
        }
        pq.insert(new Event(0, null, null));        // redraw event
        int maxPQsize = pq.size();
        // the main event-driven simulation loop
        while (!pq.isEmpty()) { 

            // get impending event, discard if invalidated
            Event e = pq.delMin();

            System.out.println("Max PQ size = " + maxPQsize);
            maxPQsize = Math.max(maxPQsize, pq.size());
            if (!e.isValid()) continue;
            ParticleQ a = e.a;
            ParticleQ b = e.b;

            // physical collision, so update positions, and then simulation clock
            for (int i = 0; i < particles.length; i++)
                particles[i].move(e.time - t);
            t = e.time;

            // process eventcolor
            if      (a != null && b != null) a.bounceOff(b);              // particle-particle collision
            else if (a != null && b == null) a.bounceOffVerticalWall();   // particle-wall collision
            else if (a == null && b != null) b.bounceOffHorizontalWall(); // particle-wall collision
            else if (a == null && b == null) redraw(limit);               // redraw event

            // update the priority queue with new collisions involving a or b
            predict(a, limit);
            predict(b, limit);

            for (int i = 0; i < particles.length; i++) {

                if (particles[i].infected && particles[i].iterationOfInfection != -1 && numberOfIterations - particles[i].iterationOfInfection >= recoveringTime) {
                    particles[i].recover();
                }
            }

        numberOfIterations++;
        }
    }   

   /***************************************************************************
    *  An event during a particle collision simulation. Each event contains
    *  the time at which it will occur (assuming no supervening actions)
    *  and the particles a and b involved.
    *
    *    -  a and b both null:      redraw event
    *    -  a null, b not null:     collision with vertical wall
    *    -  a not null, b null:     collision with horizontal wall
    *    -  a and b both not null:  binary collision between a and b
    *
    ***************************************************************************/
    private static class Event implements Comparable<Event> {
        private final double time;         // time that event is scheduled to occur
        private final ParticleQ a, b;       // particles involved in event, possibly null
        private final int countA, countB;  // collision counts at event creation
                
        
        // create a new event to occur at time t involving a and b
        public Event(double t, ParticleQ a, ParticleQ b) {
            this.time = t;
            this.a    = a;
            this.b    = b;
            if (a != null) countA = a.count();
            else           countA = -1;
            if (b != null) countB = b.count();
            else           countB = -1;
        }

        // compare times when two events will occur
        public int compareTo(Event that) {
            return Double.compare(this.time, that.time);
        }
        
        // has any collision occurred between when event was created and now?
        public boolean isValid() {
            if (a != null && a.count() != countA) return false;
            if (b != null && b.count() != countB) return false;
            return true;
        }
   
    }

    /**
     * Unit tests the {@code EpiQueue} data type.
     * Reads in the particle collision system from a standard input
     * (or generates {@code N} random particles if a command-line integer
     * is specified); simulates the system.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {

        StdDraw.setCanvasSize(600, 600);

        // enable double buffering
        StdDraw.enableDoubleBuffering();

        // the array of particles
        ParticleQ[] particles;

        /* 
          create n random particles with m (percent) suscpetibility and q (percent) quarentine
          java-introcs EpiQueue 20 60 60
        */
        int n = Integer.parseInt(args[0]);
        double m = Double.parseDouble(args[1])/100;
        double q = Double.parseDouble(args[2])/100;

        System.out.println(m);
        particles = new ParticleQ[n];
        particles[0] = new ParticleQ(true, true, false, 0); // first infected particle
      
        Counter.numberOfInfected = 1;
        
        for (int i = 1; i < n; i++) {
          boolean itsSusceptible = StdRandom.uniform(0.0, 1.0) <= m;
          boolean isFixed = StdRandom.uniform(0.0, 1.0) <= q;
          particles[i] = new ParticleQ(false, itsSusceptible, isFixed, -1); // other particles are healthy
        }
        // create collision system and simulate
        EpiQueue system = new EpiQueue(particles);
        system.simulate(10000);
    }
      
}
