/******************************************************************************
 *  Compilation:  javac ParticleSIR.java
 *  Execution:    none
 *  Dependencies: StdDraw.java
 ******************************************************************************/

import java.awt.Color;
/**
 *  Adapted from Particle.java (Sedgewick&Wayne)
 *
 *  @author Eduardo Seiji
 *  @author Gabriel Leão
 *  @author Lorenzo Follador
 */
public class ParticleSIR {
    private static final double INFINITY = Double.POSITIVE_INFINITY;

    private double rx, ry;        // position
    private double vx, vy;        // velocity
    private int count;            // collisions so far
    private final double radius;  // radius
    private final double mass;    // mass
    public Color color;    // color
    public boolean susceptible;
    public boolean infected;
    public boolean recovered;
    public boolean fixed;
    public double infectionTime;
    public int iterationOfInfection;
        
    /**
     * Initializes a particle with a random position and velocity.
     * The position is uniform in the unit box; the velocity in
     * either direciton is chosen uniformly at random.
     */
    public ParticleSIR(boolean infected, boolean susceptible, boolean fixed, int iterationOfInfection) {
        radius           = 0.01;
        rx               = StdRandom.uniform(0.0 + radius, 1.0 - radius);
        ry               = StdRandom.uniform(0.0 + radius, 1.0 - radius);
        this.fixed = fixed;
        if (!fixed) {
         vx = StdRandom.uniform(-0.005, 0.005);
          vy = StdRandom.uniform(-0.005, 0.005);
        } else {
          vx = 0;
          vy = 0;
        }
        mass             = 0.5;
        this.infected    = infected;
        this.susceptible = susceptible;
        recovered        = false;

        this.iterationOfInfection = iterationOfInfection;

        if(infected)
          color = Color.RED;
        else if (susceptible) {
          color = Color.BLUE;
        } else if (!susceptible) {
          color = Color.BLACK;
        }
    }

    /**
     * Moves this particle in a straight line (based on its velocity)
     * for the specified amount of time.
     *
     * @param  dt the amount of time
     */
    public void move(double dt) {
        rx += vx * dt;
        ry += vy * dt;
    }

    /**
     * Draws this particle to standard draw.
     */
    public void draw() {
        StdDraw.setPenColor(color);
        StdDraw.filledCircle(rx, ry, radius);

    }

    /**
     * Returns the collisions involving this particle with
     * vertical walls, horizontal walls, or other particles.
     * This is equal to the calls to {@link #bounceOff},
     * {@link #bounceOffVerticalWall}, and
     * {@link #bounceOffHorizontalWall}.
     *
     * @return the collisions involving this particle with
     *         vertical walls, horizontal walls, or other particles
     */
    public int count() {
        return count;
    }

    /**
     * Returns the amount of time for this particle to collide with the specified
     * particle, assuming no interening collisions.
     *
     * @param  that the other particle
     * @return the amount of time for this particle to collide with the specified
     *         particle, assuming no interening collisions; 
     *         {@code Double.POSITIVE_INFINITY} if the particles will not collide
     */
    public double timeToHit(ParticleSIR that) {
        if (this == that) return INFINITY;
        double dx   = that.rx - this.rx;
        double dy   = that.ry - this.ry;
        double dvx  = that.vx - this.vx;
        double dvy  = that.vy - this.vy;
        double dvdr = dx*dvx + dy*dvy;
        if (dvdr > 0) return INFINITY;
        double dvdv = dvx*dvx + dvy*dvy;
        if (dvdv == 0) return INFINITY;
        double drdr  = dx*dx + dy*dy;
        double sigma = this.radius + that.radius;
        double d     = (dvdr*dvdr) - dvdv * (drdr - sigma*sigma);
        // if (drdr < sigma*sigma) StdOut.println("overlapping particles");
        if (d < 0) return INFINITY;
        return -(dvdr + Math.sqrt(d)) / dvdv;
    }

    /**
     * Returns the amount of time for this particle to collide with a vertical
     * wall, assuming no interening collisions.
     *
     * @return the amount of time for this particle to collide with a vertical wall,
     *         assuming no interening collisions; 
     *         {@code Double.POSITIVE_INFINITY} if the particle will not collide
     *         with a vertical wall
     */
    public double timeToHitVerticalWall() {
        if      (vx > 0) return (1.0 - rx - radius) / vx;
        else if (vx < 0) return (radius - rx) / vx;  
        else             return INFINITY;
    }

    /**
     * Returns the amount of time for this particle to collide with a horizontal
     * wall, assuming no interening collisions.
     *
     * @return the amount of time for this particle to collide with a horizontal wall,
     *         assuming no interening collisions; 
     *         {@code Double.POSITIVE_INFINITY} if the particle will not collide
     *         with a horizontal wall
     */
    public double timeToHitHorizontalWall() {
        if      (vy > 0) return (1.0 - ry - radius) / vy;
        else if (vy < 0) return (radius - ry) / vy;
        else             return INFINITY;
    }

    /**
     * Updates the velocities of this particle and the specified particle according
     * to the laws of elastic collision. Assumes that the particles are colliding
     * at this instant.
     *
     * @param  that the other particle
     */
    public void bounceOff(ParticleSIR that) {
        double dx   = that.rx - this.rx;
        double dy   = that.ry - this.ry;
        double dvx  = that.vx - this.vx;
        double dvy  = that.vy - this.vy;
        double dvdr = dx*dvx + dy*dvy;             // dv dot dr
        double dist = this.radius + that.radius;   // distance between particle centers at collison

        // magnitude of normal force
        double magnitude = 2 * this.mass * that.mass * dvdr / ((this.mass + that.mass) * dist);

        // normal force, and in x and y directions
        double fx = magnitude * dx / dist;
        double fy = magnitude * dy / dist;

        // update velocities according to normal force
        if (!fixed && !that.fixed) {
            this.vx += fx / this.mass;
            this.vy += fy / this.mass;
            that.vx -= fx / that.mass;
            that.vy -= fy / that.mass;
        }
        else if (fixed && !that.fixed)
        {
            magnitude = 2 * that.mass * dvdr / (dist);
            fx = magnitude * dx / dist;
            fy = magnitude * dy / dist;

            that.vx -= fx / that.mass;
            that.vy -= fy / that.mass;
        }
        else if (!fixed && that.fixed)
        {
            magnitude = 2 * this.mass * dvdr / (dist);
            fx = magnitude * dx / dist;
            fy = magnitude * dy / dist;

            this.vx += fx / this.mass;
            this.vy += fy / this.mass;
        }

        // update collision counts
        this.count++;
        that.count++;

        // updates color and add a recover event to the pq
        if((this.infected || that.infected) && (this.susceptible && that.susceptible) && !(this.infected && that.infected)) {
            
            if (that.infected) {
                this.iterationOfInfection = EpiSim.numberOfIterations;
            }
            if (this.infected) {
                that.iterationOfInfection = EpiSim.numberOfIterations;
            }      

            this.color    = Color.RED;
            this.infected = true;
            that.color    = Color.RED;
            that.infected = true;

            Counter.numberOfInfected++;
            Counter.numberOfSusceptible--;  
        }
    }

    /**
     * Updates the velocity of this particle upon collision with a vertical
     * wall (by reflecting the velocity in the <em>x</em>-direction).
     * Assumes that the particle is colliding with a vertical wall at this instant.
     */
    public void bounceOffVerticalWall() {
        vx = -vx;
        count++;
    }

    /**
     * Updates the velocity of this particle upon collision with a horizontal
     * wall (by reflecting the velocity in the <em>y</em>-direction).
     * Assumes that the particle is colliding with a horizontal wall at this instant.
     */
    public void bounceOffHorizontalWall() {
        vy = -vy;
        count++;
    }

    /**
     * Returns the kinetic energy of this particle.
     * The kinetic energy is given by the formula 1/2 <em>m</em> <em>v</em><sup>2</sup>,
     * where <em>m</em> is the mass of this particle and <em>v</em> is its velocity.
     *
     * @return the kinetic energy of this particle
     */
    public double kineticEnergy() {
        return 0.5 * mass * (vx*vx + vy*vy);
    }

    // recover the infected particle
    public void recover() {
        this.color     = Color.GREEN;
        this.infected  = false;
        this.recovered = true;
        this.susceptible = false;
        Counter.numberOfInfected--;
        Counter.numberOfRecovered++;
    }
}