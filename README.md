## EpiSim: um simples simulador de epidemias

---
#### EpiSim é capaz de simular e representar a progressão de uma epidemia através de *event-driven programming* e um simples visualizador. O programa derivou de [CollisionSystem.java](https://algs4.cs.princeton.edu/code/javadoc/edu/princeton/cs/algs4/CollisionSystem.html), de Sedgewick&Wayne.

![EpiSim se comporta assim.](https://gitlab.com/USPlorenzofollador/episim/-/raw/master/issuesData/EpiSimExample.png)
---
Colaboradores: [Eduardo Seiji](https://gitlab.com/eduardoseiji98), [Gabriel Souza](https://gitlab.com/Beserker)

Dependencies: [StdDraw](https://introcs.cs.princeton.edu/java/stdlib/javadoc/StdDraw.html), [StdRandom](https://algs4.cs.princeton.edu/code/javadoc/edu/princeton/cs/algs4/StdRandom.html), [Draw](https://introcs.cs.princeton.edu/java/stdlib/javadoc/Draw.html), [MinPQ](https://algs4.cs.princeton.edu/61event/MinPQ.java)


---
A execução é da seguinte forma:
 ```
 java-introcs EpiSim <número de partículas> <porcentagem susceptibilidade> <porcentagem de distanciamento>
 ```
Altenativamente, é possível escolher o tempo de recuperação (padrão = 1000):
```
 java-introcs EpiSim <número de partículas> <porcentagem susceptibilidade> <porcentagem de distanciamento> <tempo de recuperação>
 ```


Exemplo de execução: 
```
$ java-introcs EpiSim 200 80 46
```
Nesse caso, duzentas partículas são desenhadas, com 80% de susceptibilidade e 46% de índice de distanciamento.

```
$ java-introcs EpiSim 300 90 70 500
```
Nesse caso, trezentas partículas são desenhadas, com 90% de susceptibilidade, 70% de índice de distanciamento e metade do tempo padrão de recuperação.

Bolinhas azuis são susceptíveis de infecção.\
Bolinhas vermelhas estão infectadas.\
Bolinhas verdes foram recuperadas.\
Bolinhas pretas são imunes.

O pequeno gráfico mostra a curva de infecção ao longo do tempo. As infecções ativas são representadas pela curva vermelha de baixo, e os 
recuperados pela curva verde acima.

O programa também imprime na tela do console os números de infecções ativas, de susceptíveis, de recuperados, de imunes e de casos totais.

---

### Considerações
O projeto iniciou considerando que nem todas as bolinhas são vulneráveis à infecção. Isso é importante porque determina uma redução no contágio por unidade de tempo, fator crítico para se evitar o colapso da rede hospitalar. Aprimoramentos pretendidos incluem a "testagem" de uma amostra e a comparação com os números totais, e o impacto do distanciamento na "visita" das bolinhas a uma determinada área do canvas, simulando a redução na atividade econômica.

Verifica-se claramente a diferença das curvas de infecção em cenários diferentes de distanciamento social (exemplo com 80% de susceptibilidade):

![0% de distanciamento](https://gitlab.com/USPlorenzofollador/episim/-/raw/master/issuesData/0%25%20distancing.png)\
*0% de distanciamento*

![46% de distanciamento (média para SP)](https://gitlab.com/USPlorenzofollador/episim/-/raw/master/issuesData/46%25%20distancing.png)\
*46% de distanciamento (média para SP)*

A "imunidade de rebanho" também pode ser incorporada na simulação alterando-se a susceptibilidade. Ela é importante para evitar novos surtos da doença. Quanto menor a susceptibilidade, mais achatada fica a curva. 

Também é interessante notar que, em algumas simulações, a epidemia termina e ainda existem bolinhas azuis na tela (contabilizadas pelo número de `Susceptible`). Isso significa que essas bolinhas não foram infectadas. Elas poderiam representar as pessoas com comorbidades ou que são especialmente vulneráveis à doença infecciosa, por exemplo. Observamos, por repetidas simulações, que essas bolinhas são mais numerosas quando o distanciamento é maior e a susceptibilidade é menor. Ou seja, elas foram poupadas pelo distanciamento ou pela imunidade de rebanho. Uma questão interessante seria determinar uma correlação estatística entre a variação de "poupados" e o índice de distanciamento social.\
Em uma simulação particular, `EpiSim 200 100 90` (todos são vulneráveis e o distanciamento é altíssimo), observou-se uma segunda onda de infecções. 
![`EpiSim 200 100 90`](https://gitlab.com/USPlorenzofollador/episim/-/raw/master/issuesData/EpiSim%20200%20100%2090.png)
Isso é extremamente interessante, porque mostra a vulnerabilidade de uma proposta de contenção da doença pobre em medidas alternativas. Em pelo menos um cenário, o isolamento se mostrou incapaz de conter um grande crescimento de infecções ativas simultâneas, o que poderia, mesmo assim, acarretar o colapso do sistema de saúde.
Particularmente, foram encontradas "duas ondas" com mais frequência nas simulações com 300 partículas, 100% de susceptibilidade e 90% de distanciamento. O [vídeo a seguir](https://www.youtube.com/watch?v=G_lWKkBBPec) demonstra esse comportamento. Também podemos vê-lo no [segundo vídeo](https://www.youtube.com/watch?v=4yDmskta-gc).
### Desempenho

__EpiSim.java__ apresentou um desempenho variável com o número de bolinhas desenhadas. Em uma máquina de teste, a diferença entre simulações de 100, 200 e 300 bolinhas foi notável (para 300, a máquina travou!). Isso porque a fila de prioridades é altamente variável entre esses casos. Interessantemente, para __CollisionSystem.java__ o número de bolinhas parece mais crítico do que para __EpiSim.java__! 

Para rodar __CollisionSystemMod.java__ (__CollisionSystem.java__ que mostra a __PQ__), faça ```java-introcs CollisionSystemMod n d``` em que `n` é o número de partículas e `d` é o tempo limite de simulação.
A modificação feita em __CollisionSystemMod.java__ permite verificar o tamanho da __PQ__. Uma execução de `java-introcs CollisionSystemMod 100 10`, retornou `Max PQ size = 102`. A execução de `java-introcs CollisionSystemMod 200 10` retorna `Max PQ size = 112509`.\
Um detalhe também interessante é que __CollisionSystemMod.java__ possui inconsistências. Por exemplo, mesmo restringindo a área de "nascimento" das bolinhas, [algumas ainda "nascem" para fora do canvas](https://gitlab.com/USPlorenzofollador/episim/-/blob/master/issuesData/bolinhas.png). Isso pode ser demonstrado observando-se a moldura vermelha que foi colocada no código original e rodando ```java-introcs CollisionSystemMod 200 0.1```

__EpiQueue.java__ pode ser usado analogamente para analisar a fila que efetivamente é usada na simulação da epidemia. Para rodar __EpiQueue.java__, proceda como __EpiSim.java__.

[Playlist com algumas simulações](https://www.youtube.com/playlist?list=PLhlxWAb8YPCeIelL0Cily3BcCfMENbMgO)
---